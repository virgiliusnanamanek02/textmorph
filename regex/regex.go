package regex

import "regexp"

// Match

func Match(pattern, str string) (bool, error) {
	matched, err := regexp.MatchString(pattern, str)
	if err != nil {
		return false, err
	}
	return matched, nil
}

// Replace

func Replace(pattern, replacement, str string) (string, error) {
	re, err := regexp.Compile(pattern)
	if err != nil {
		return "", err
	}
	return re.ReplaceAllString(str, replacement), nil
}
