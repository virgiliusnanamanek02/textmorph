package regex

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMatch(t *testing.T) {
	tests := []struct {
		name    string
		pattern string
		str     string
		want    bool
		wantErr bool
	}{
		{
			name:    "Test Matching",
			pattern: `\d+`,
			str:     "12345",
			want:    true,
			wantErr: false,
		},
		{
			name:    "Test Non-Matching",
			pattern: `\d+`,
			str:     "abcde",
			want:    false,
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Match(tt.pattern, tt.str)
			if (err != nil) != tt.wantErr {
				t.Errorf("Match() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			assert.Equal(t, tt.want, got, "Match() got = %v, want %v", got, tt.want)
		})
	}
}

func TestReplace(t *testing.T) {
	tests := []struct {
		name        string
		pattern     string
		replacement string
		str         string
		want        string
		wantErr     bool
	}{
		{
			name:        "Test Replacement",
			pattern:     `\d+`,
			replacement: "#",
			str:         "There are 123 apples and 45 oranges.",
			want:        "There are # apples and # oranges.",
			wantErr:     false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Replace(tt.pattern, tt.replacement, tt.str)
			if (err != nil) != tt.wantErr {
				t.Errorf("Replace() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			assert.Equal(t, tt.want, got, "Replace() got = %v, want %v", got, tt.want)
		})
	}
}
