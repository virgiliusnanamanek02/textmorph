package strings

import (
	"testing"
)

func TestConcat(t *testing.T) {
	tests := []struct {
		name string
		s    []string
		want string
	}{
		{
			name: "Concatenate Strings",
			s:    []string{"Hello", " ", "World"},
			want: "Hello World",
		},
		{
			name: "Concatenate Empty Strings",
			s:    []string{},
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Concat(tt.s...); got != tt.want {
				t.Errorf("Concat() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestToLowerCase(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want string
	}{
		{
			name: "Convert to Lowercase",
			s:    "Hello World",
			want: "hello world",
		},
		{
			name: "Convert Empty String to Lowercase",
			s:    "",
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ToLowerCase(tt.s); got != tt.want {
				t.Errorf("ToLowerCase() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestToUpperCase(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want string
	}{
		{
			name: "Convert to Uppercase",
			s:    "hello world",
			want: "HELLO WORLD",
		},
		{
			name: "Convert Empty String to Uppercase",
			s:    "",
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ToUpperCase(tt.s); got != tt.want {
				t.Errorf("ToUpperCase() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTrim(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want string
	}{
		{
			name: "Trim Spaces",
			s:    "   Hello World   ",
			want: "Hello World",
		},
		{
			name: "Trim Empty String",
			s:    "",
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Trim(tt.s); got != tt.want {
				t.Errorf("Trim() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCapitalize(t *testing.T) {

	tests := []struct {
		name string
		s    string
		want string
	}{
		{
			name: "Capitalize S",
			s:    "hello world one",
			want: "Hello World One",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Capitalize(tt.s); got != tt.want {
				t.Errorf("Capitalize() = %v, want %v", got, tt.want)
			}
		})
	}
}
