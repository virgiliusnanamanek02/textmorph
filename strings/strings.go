package strings

import (
	"strings"
	"unicode"
)

// Concat

func Concat(s ...string) string {
	return strings.Join(s, "")
}

// Trim

func Trim(s string) string {
	return strings.TrimSpace(s)
}

// To Upper

func ToUpperCase(s string) string {
	return strings.ToUpper(s)
}

// To Lower

func ToLowerCase(s string) string {
	return strings.ToLower(s)
}

// Capitalize

func Capitalize(s string) string {
	s = strings.ToLower(s)
	var resultStr []rune
	uppercaseNext := true

	for _, r := range s {
		if unicode.IsSpace(r) {
			uppercaseNext = true
			resultStr = append(resultStr, r)
		} else if uppercaseNext {
			resultStr = append(resultStr, unicode.ToUpper(r))
			uppercaseNext = false
		} else {
			resultStr = append(resultStr, unicode.ToLower(r))
		}
	}

	return string(resultStr)
}
