package sanitize

import (
	"testing"
)

func TestRemoveHTMLTag(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want string
	}{
		{
			name: "Remove HTML Tags",
			s:    "<p>This is a <b>test</b>.</p>",
			want: "This is a test.",
		},
		{
			name: "Empty String",
			s:    "",
			want: "",
		},
		{
			name: "String with No HTML Tags",
			s:    "Hello World",
			want: "Hello World",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RemoveHTMLTag(tt.s); got != tt.want {
				t.Errorf("RemoveHTMLTag() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTrimWhitespace(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want string
	}{
		{
			name: "Trim Whitespace",
			s:    "   Hello World   ",
			want: "Hello World",
		},
		{
			name: "Empty String",
			s:    "",
			want: "",
		},
		{
			name: "No Whitespace",
			s:    "Hello",
			want: "Hello",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := TrimWhitespace(tt.s); got != tt.want {
				t.Errorf("TrimWhitespace() = %v, want %v", got, tt.want)
			}
		})
	}
}
