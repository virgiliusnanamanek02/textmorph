package sanitize

import (
	"regexp"
	"strings"
)

// Remove HTMLTags

func RemoveHTMLTag(s string) string {
	re := regexp.MustCompile("<[^>]*>")
	return re.ReplaceAllString(s, "")
}

// Trim White Space

func TrimWhitespace(s string) string {
	return strings.TrimSpace(s)
}
