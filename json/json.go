package json

import "encoding/json"

func Parse(jsonString string, v interface{}) error {
	return json.Unmarshal([]byte(jsonString), v)
}

func Serialize(v interface{}) (string, error) {
	b, err := json.Marshal(v)
	if err != nil {
		return "", err
	}

	return string(b), nil
}
