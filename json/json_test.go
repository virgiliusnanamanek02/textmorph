package json

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParse(t *testing.T) {
	type args struct {
		jsonString string
		v          interface{}
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Valid JSON",
			args: args{
				jsonString: `{"name": "John", "age": 30}`,
				v:          &map[string]interface{}{},
			},
			wantErr: false,
		},
		{
			name: "Invalid JSON",
			args: args{
				jsonString: `{"name": "John", "age": 30`,
				v:          &map[string]interface{}{},
			},
			wantErr: true,
		},
		{
			name: "Empty JSON",
			args: args{
				jsonString: ``,
				v:          &map[string]interface{}{},
			},
			wantErr: true,
		},
		{
			name: "Nested JSON",
			args: args{
				jsonString: `{"person": {"name": "John", "age": 30}}`,
				v:          &map[string]interface{}{},
			},
			wantErr: false,
		},
		{
			name: "Array JSON",
			args: args{
				jsonString: `{"names": ["John", "Doe"]}`,
				v:          &map[string]interface{}{},
			},
			wantErr: false,
		},
		{
			name: "Malformed JSON",
			args: args{
				jsonString: `{"name": "John", "age": "thirty"`,
				v:          &map[string]interface{}{},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := Parse(tt.args.jsonString, tt.args.v)
			if tt.wantErr {
				assert.Error(t, err, "Parse() should return an error")
			} else {
				assert.NoError(t, err, "Parse() should not return an error")
			}
		})
	}
}

func TestSerialize(t *testing.T) {
	type args struct {
		v interface{}
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Valid Map",
			args: args{
				v: map[string]interface{}{
					"name": "John",
					"age":  30,
				},
			},
			want:    `{"name":"John","age":30}`,
			wantErr: false,
		},
		{
			name: "Invalid Type",
			args: args{
				v: func() {},
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Empty Map",
			args: args{
				v: map[string]interface{}{},
			},
			want:    `{}`,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Serialize(tt.args.v)
			if tt.wantErr {
				assert.Error(t, err, "Serialize() should return an error")
			} else {
				assert.NoError(t, err, "Serialize() should not return an error")
				assert.JSONEq(t, tt.want, got, "Serialize() got = %v, want %v", got, tt.want)
			}
		})
	}
}
