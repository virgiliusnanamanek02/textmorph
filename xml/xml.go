package xml

import (
	"encoding/xml"
	"errors"
	"reflect"
)

func Parse(xmlStr string, v interface{}) error {
	return xml.Unmarshal([]byte(xmlStr), v)
}

// Serialize converts a struct to its XML representation.
func Serialize(v interface{}) (string, error) {
	if v == nil {
		return "", errors.New("input cannot be nil")
	}

	// Check if v is a struct
	val := reflect.ValueOf(v)
	if val.Kind() != reflect.Struct {
		return "", errors.New("unsupported type: input must be a struct")
	}

	// Marshal struct to XML
	xmlData, err := xml.Marshal(v)
	if err != nil {
		return "", err
	}

	return string(xmlData), nil
}
