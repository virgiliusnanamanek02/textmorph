package xml

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParse(t *testing.T) {
	type Person struct {
		Name string `xml:"Name"`
		Age  int    `xml:"Age"`
	}

	tests := []struct {
		name    string
		xmlStr  string
		v       interface{}
		wantErr bool
	}{
		{
			name:    "Valid XML",
			xmlStr:  "<Person><Name>John Doe</Name><Age>30</Age></Person>",
			v:       &Person{},
			wantErr: false,
		},
		{
			name:    "Invalid XML",
			xmlStr:  "<Person><Name>John Doe</Name><Age>thirty</Age></Person>",
			v:       &Person{},
			wantErr: true,
		},
		{
			name:    "Empty XML",
			xmlStr:  "",
			v:       &Person{},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := Parse(tt.xmlStr, tt.v)
			if (err != nil) != tt.wantErr {
				t.Errorf("Parse() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestSerialize(t *testing.T) {
	type Person struct {
		Name string `xml:"Name"`
		Age  int    `xml:"Age"`
	}

	tests := []struct {
		name    string
		v       interface{}
		want    string
		wantErr bool
	}{
		{
			name:    "Serialize Struct",
			v:       Person{Name: "John Doe", Age: 30},
			want:    "<Person><Name>John Doe</Name><Age>30</Age></Person>",
			wantErr: false,
		},
		{
			name:    "Serialize Nil",
			v:       nil,
			want:    "",
			wantErr: true,
		},
		{
			name:    "Serialize Invalid Type",
			v:       "John Doe", // Not a struct
			want:    "",
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Serialize(tt.v)
			if (err != nil) != tt.wantErr {
				t.Errorf("Serialize() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			assert.Equal(t, tt.want, got, "Serialize() got = %v, want %v", got, tt.want)
		})
	}
}
