# TextMorph - Go Library for Text Parsing and Manipulation

TextMorph is a Go library that provides various functionalities for parsing and manipulating text effectively. With TextMorph, you can easily parse JSON/XML, use regular expressions, manipulate strings, and sanitize text.

## Features

- **JSON Parsing and Serialization**: Parse JSON strings into Go data structures and serialize Go data structures into JSON.
- **XML Parsing and Serialization**: Parse XML strings into Go data structures and serialize Go data structures into XML.
- **Regular Expressions**: Matching, replacing, and validating text using regular expressions.
- **String Manipulation**: String operations like concatenation, trimming, case conversion, etc.
- **Text Sanitization**: Cleanse text by removing unwanted characters or elements like HTML tags.
- More...

## Installation

To use TextMorph, you can install the library using Go Modules. Run the following command in your terminal:

```bash
go get gitlab.com/virgiliusnanamanek02/textmorph
```

## Usage

Here are some examples of using the core functionalities of TextMorph:

### JSON Parsing and Serialization

```go
package main

import (
    "fmt"
    "gitlab.com/virgiliusnanamanek02/textmorph/json"
)

func main() {
    jsonStr := `{"name": "John", "age": 30}`
    var data map[string]interface{}
    err := json.Parse(jsonStr, &data)
    if err != nil {
        fmt.Println("Error parsing JSON:", err)
        return
    }
    fmt.Println("Parsed JSON:", data)

    serializedJSON, err := json.Serialize(data)
    if err != nil {
        fmt.Println("Error serializing JSON:", err)
        return
    }
    fmt.Println("Serialized JSON:", serializedJSON)
}
```

### Regular Expressions

```go
package main

import (
    "fmt"
	"gitlab.com/virgiliusnanamanek02/textmorph/regex"
)

func main() {
    pattern := `\d+`
    text := "There are 123 apples and 45 oranges."
    matched, err := regex.Match(pattern, text)
    if err != nil {
        fmt.Println("Error matching regex:", err)
        return
    }
    fmt.Println("Regex matched:", matched)

    replacedText, err := regex.Replace(pattern, "#", text)
    if err != nil {
        fmt.Println("Error replacing text:", err)
        return
    }
    fmt.Println("Replaced text:", replacedText)
}
```

### String Manipulation

```go
package main

import (
    "fmt"
	"gitlab.com/virgiliusnanamanek02/textmorph/stringers"
)

func main() {
    concatenated := stringers.Concat("Hello", " ", "World")
    fmt.Println("Concatenated string:", concatenated)

    trimmed := stringers.Trim("   Hello World   ")
    fmt.Println("Trimmed string:", trimmed)

    upper := stringers.ToUpper("hello")
    fmt.Println("Upper case string:", upper)

    lower := stringers.ToLower("HELLO")
    fmt.Println("Lower case string:", lower)
}
```

### Text Sanitization

```go
package main

import (
    "fmt"
	"gitlab.com/virgiliusnanamanek02/textmorph/sanitize"
)

func main() {
    dirtyText := "<p>This is a <b>test</b>.</p>"
    cleanText := sanitize.RemoveHTMLTags(dirtyText)
    fmt.Println("Clean text:", cleanText)

    whitespaceText := "   Hello World   "
    cleanWhitespace := sanitize.TrimWhitespace(whitespaceText)
    fmt.Println("Trimmed whitespace text:", cleanWhitespace)
}
```
## Contributing

Contributions and feedback for the development of TextMorph are welcome. Please open an issue or pull request on the [GitLab repository](https://gitlab.com/virgiliusnanamanek02/textmorph.git).

## Branching Strategy

We follow a branching strategy to keep our codebase organized and to streamline collaboration. Here’s how we structure our branches:

### Main Branches

- **`main`**: The stable branch containing production-ready code.
- **`develop`**: The integration branch for the latest development changes. New features and fixes are merged here before being included in the `main` branch.

### Supporting Branches

- **Feature Branches**: Used for developing new features. Typically branched off from `develop`.
    - Naming convention: `feature/feature-name`
    - Example: `feature/add-regex-support`
- **Bugfix Branches**: Used for fixing bugs. Typically branched off from `develop` or `main`.
    - Naming convention: `bugfix/bug-name`
    - Example: `bugfix/fix-regex-issue`
- **Release Branches**: Used for preparing a new production release. Branched off from `develop`.
    - Naming convention: `release/x.y.z`
    - Example: `release/1.0.0`
- **Hotfix Branches**: Used for quickly fixing critical issues in the production code. Branched off from `main`.
    - Naming convention: `hotfix/hotfix-name`
    - Example: `hotfix/urgent-fix`

## How to Contribute

We welcome contributions from everyone. Here’s how you can help:

1. **Fork the Repository**: Click the 'Fork' button in the top right corner of this repository to create a copy in your own GitLab account.

2. **Clone Your Fork**: Clone your forked repository to your local machine.
   ```sh
   git clone https://gitlab.com/virgiliusnanamanek02/textmorph.git
   cd textmorph
   ```

3. **Create a Feature Branch**: Create a new branch for your feature or bugfix.
   ```sh
   git checkout develop
   git pull origin develop
   git checkout -b feature/your-feature-name
   ```

4. **Make Your Changes**: Implement your feature or fix the bug. Ensure your code follows the project's coding standards and includes necessary tests.

5. **Commit Your Changes**: Commit your changes with a clear and descriptive commit message.
   ```sh
   git add .
   git commit -m "Add detailed description of what you have done Ref #[issue's number]"
   ```

6. **Push Your Branch**: Push your branch to your forked repository.
   ```sh
   git push origin feature/your-feature-name
   ```

7. **Create a Merge Request (MR)**: Go to your forked repository on GitLab, switch to your feature branch, and click on 'Merge Request'. Fill in the details and submit your MR for review.

8. **Review and Address Feedback**: Your MR will be reviewed by project maintainers. Make sure to address any feedback or requested changes.

9. **Merge Your MR**: Once your MR is approved, it will be merged into the `develop` branch. If you have permissions, you can merge it yourself; otherwise, a maintainer will merge it for you.

10. **Keep Your Fork Updated**: Regularly sync your fork with the upstream repository to stay up-to-date with the latest changes.
    ```sh
    git remote add upstream https://gitlab.com/virgiliusnanamanek02/textmorph.git
    git fetch upstream
    git checkout develop
    git merge upstream/develop
    git push origin develop
    ```

By following these steps, you can help improve the project while ensuring a smooth workflow for everyone involved. If you have any questions or need help, feel free to open an issue or contact the maintainers.

## License

TextMorph is licensed under the MIT License. See [LICENSE](LICENSE) for more information.